import React from "react";
import { Modal, Button, Typography, Row, Col, Image, Space } from "antd";
import { useSelector, useDispatch } from "react-redux";
import { hideModal } from "../../../../redux/actions/modalAction";

const { Text } = Typography;

const ModalComponent = () => {
  const state = useSelector((state) => {
    return state.modalReducer;
  }); // get redux store

  const isModalVisible = state.showModal;
  const modalData = state.modalData;
  const dispatch = useDispatch();

  const closeModal = () => {
    dispatch(hideModal());
  };

  return (
    <>
      <Modal
        title={isModalVisible && modalData.name}
        visible={isModalVisible}
        onOk={closeModal}
        onCancel={closeModal}
        footer={[
          <Button key='back' onClick={closeModal} type='primary'>
            Close
          </Button>,
        ]}
      >
        {isModalVisible && (
          <Row gutter={16}>
            <Col span={8}>
              <Image src={modalData.picture_large} />
            </Col>
            <Col span={16}>
              <Space direction='vertical'>
                <Text type='secondary'>
                  <b>Gender:</b> {modalData.gender.toUpperCase()}
                </Text>
                <Text type='secondary'>
                  <b>DOB:</b> {modalData.dob}
                </Text>
                <Text type='secondary'>
                  <b>Email:</b> {modalData.email}
                </Text>
                <Text type='secondary'>
                  <b>Address:</b> {modalData.address}
                </Text>
              </Space>
            </Col>
          </Row>
        )}
      </Modal>
    </>
  );
};

export default ModalComponent;
