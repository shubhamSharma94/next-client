import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  createNewUser,
  deleteAllUserList,
  getUserList,
} from "../../../redux/actions/userAction";
import { List, Button } from "antd";
import ListItem from "../../atom/ListItem/ListItem";
import _ from "underscore";
import { Typography, Spin } from "antd";
const { Title } = Typography;

const UserList = (props) => {
  const state = useSelector((state) => {
    return state.userReducer;
  }, _.isEqual); // get redux store
  const dispatch = useDispatch();

  useEffect(() => {
    const fetchUserList = () => {
      dispatch(getUserList());
    };
    fetchUserList();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []); // ComponentDidMount

  let userList = state.userList;
  let isloading = state.status === "waiting";
  return (
    <>
      <List
        className=''
        header={
          <div>
            <Title level={2}>User List</Title>
            <span>
              <Button
                type='primary'
                onClick={() => {
                  dispatch(createNewUser());
                }}
                disabled={isloading}
              >
                {isloading ? <Spin /> : ""}
                Add More
              </Button>
              <Button
                type='primary'
                danger
                onClick={() => {
                  dispatch(deleteAllUserList());
                }}
                style={{ float: "right" }}
                disabled={isloading}
              >
                {isloading ? <Spin /> : ""}
                Clear All
              </Button>
            </span>
          </div>
        }
        itemLayout='horizontal'
        size='large'
        dataSource={userList}
        pagination={{
          onChange: (page) => {
            console.log(page);
          },
          pageSize: 10,
        }}
        renderItem={(item) => <ListItem key={item.id} {...item} />}
      />
    </>
  );
};

export default UserList;
